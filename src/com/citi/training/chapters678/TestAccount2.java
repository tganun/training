package com.citi.training.chapters678;

public class TestAccount2 {
    public static void main(String[] args){
        Account.setInterestRate(1.10);
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23, 5444, 2, 345, 34};
        String[] names = {"Picard", "Ryker", "Worf","Troy", "Data"};
        for(int i=0; i< arrayOfAccounts.length;i++){
            arrayOfAccounts[i] = new Account(amounts[i], names[i]);
            System.out.println("Before Interest   | Account " + i + ": Name: " + arrayOfAccounts[i].getName() + " Amount: $" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println("After Interest    | Account " + i + ": Name: " + arrayOfAccounts[i].getName() + " Amount: $" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].withdraw(40);
            System.out.println("After withdraw 1  | Account " + i + ": Name: " + arrayOfAccounts[i].getName() + " Amount: $" + arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].withdraw();
            System.out.println("After withdraw 2  | Account " + i + ": Name: " + arrayOfAccounts[i].getName() + " Amount: $" + arrayOfAccounts[i].getBalance());
        }
    }
}
