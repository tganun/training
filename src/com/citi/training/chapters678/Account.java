package com.citi.training.chapters678;

public class Account {

    private static double interestRate;
    private double balance;
    private String name;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public String getName() {
        return name;
    }

    public void addInterest(){
        balance = getBalance();
        setBalance(balance = balance*interestRate);
    }

    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }

    public Boolean withdraw(double amt){
        if((balance - amt) >= 0){
            System.out.println("Successfully removed $" + amt);
            return true;
        }else{
            System.out.println("You are too broke to remove money");
            return false;
        }
    }

    public Boolean withdraw(){
        if((balance - 20) >= 0){
            System.out.println("Successfully removed $" + 20);
            return true;
        }else{
            System.out.println("You are too broke to remove money");
            return false;
        }
    }

    public Account(){
        this (50,"Tim");
    }
}
