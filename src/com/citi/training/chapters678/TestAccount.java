package com.citi.training.chapters678;

public class TestAccount {

    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {23, 5444, 2, 345, 34};
        String[] names = {"Picard", "Ryker", "Worf","Troy", "Data"};
        for(int i=0; i< arrayOfAccounts.length;i++){
            arrayOfAccounts[i] = new Account(amounts[i], names[i]);
            System.out.println("Account " + i + ": Name: " + names[i] + " Amount: " + amounts[i]);
        }
    }
}
