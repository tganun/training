package com.citi.training.chapter11;

public class TestInheritance {
    public static void main(String[] args){
        Account[] account = new Account[3];
        account[0] = new SavingsAccount("Account", 2);
        account[1] = new SavingsAccount("SavingsAccount", 4);
        account[2] = new CurrentAccount("CurrentAccount", 6);

        for(int i=0;i<account.length;i++){
            account[i].addInterest();
            System.out.println("New balance: $" + account[i].getBalance() + " in account : " + account[i].getName());
        }
    }
}
