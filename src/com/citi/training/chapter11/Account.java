package com.citi.training.chapter11;

public abstract class Account {

    private double balance;
    private String name;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public Account(String name, double balance) {
        this.balance = balance;
        this.name = name;
    }

    public Account(){
        this ("Tim", 50);
    }
}
