package com.citi.training.chapter121314;

public interface Detailable {

    String getDetails();
}
