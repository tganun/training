package com.citi.training.chapter121314;

import java.util.Comparator;
import java.util.HashSet;
import java.util.TreeSet;

public class AccountComparator implements Comparator {

    public static void main(String[] args) {
        TreeSet<Account> accounts = new TreeSet<>();
        Account acc1 = new SavingsAccount("Tim", 10);
        Account acc2 = new CurrentAccount("Current", 25);
        Account acc3 = new SavingsAccount("Savings", 1000);
        accounts.add(acc1);
        accounts.add(acc2);
        accounts.add(acc3);
    }

    @Override
    public int compare(Object o1, Object o2) {
        return 0;
    }
}