package com.citi.training.chapter121314;

public class TestInterfaces {
    public static void main(String[] args){
        Detailable[] detailable = new Detailable[3];
        detailable[0] = new HomeInsurance(10, 10, 2);
        detailable[1] = new SavingsAccount("SavingsAccount", 4);
        detailable[2] = new CurrentAccount("CurrentAccount", 6);

        for(int i=0;i<detailable.length;i++){
            detailable[i].getDetails();
        }
    }
}
