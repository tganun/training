package com.citi.training.chapter121314;

public class HomeInsurance implements Detailable{
    private double amountInsured;
    private double excess;
    private double premium;

    public HomeInsurance(double amountInsured, double excess, double premium){
        this.amountInsured = amountInsured;
        this.excess = excess;
        this.premium = premium;
    }


    public String getDetails() {
        return "" + premium + " " + excess;
    }
}
