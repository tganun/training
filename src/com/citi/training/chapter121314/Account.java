package com.citi.training.chapter121314;

public abstract class Account implements Detailable{

    private double balance;
    private String name;

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    public Account(String name, double balance) {
        this.balance = balance;
        this.name = name;
    }

    public Account(){
        this ("Tim", 50);
    }

    public String getDetails(){
        return "Details got";
    }
}
