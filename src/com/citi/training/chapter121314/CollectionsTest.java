package com.citi.training.chapter121314;

import java.util.HashSet;

public class CollectionsTest {
    public static void main(String[] args){
        HashSet<Account> accounts = new HashSet<>();
        Account acc1 = new SavingsAccount("Tim", 10);
        Account acc2 = new CurrentAccount("Current", 25);
        Account acc3 = new SavingsAccount("Savings", 1000);
        accounts.add(acc1);
        accounts.add(acc2);
        accounts.add(acc3);

        for(Account a: accounts){
            a.getName();
            a.getBalance();
            a.addInterest();
            a.getBalance();
        }
    }
}
